import { CheckInVoucherPage } from './../pages/check-in-voucher/check-in-voucher';

import { InvoicePage } from './../pages/invoice/invoice';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { OrderDetailMorePage } from '../pages/order-detail-more/order-detail-more';
import { ConfirmationPage } from '../pages/confirmation/confirmation';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = HomePage;
      rootPage:any = OrderDetailMorePage;
  // rootPage:any = CheckInVoucherPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
