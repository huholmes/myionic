import { CheckInVoucherPage } from './../pages/check-in-voucher/check-in-voucher';
import { InvoicePage } from './../pages/invoice/invoice';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { OrderDetailMorePage } from './../pages/order-detail-more/order-detail-more';
import { OrderDetailPage } from './../pages/order-detail/order-detail';
import { PayMethodPage } from './../pages/pay-method/pay-method';
import { ConfirmationPage } from '../pages/confirmation/confirmation';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PayMethodPage,
    OrderDetailPage,
    OrderDetailMorePage,
    ConfirmationPage,
    InvoicePage,
    CheckInVoucherPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    OrderDetailPage,
    OrderDetailMorePage,
    PayMethodPage,
    ConfirmationPage,
    InvoicePage,
    CheckInVoucherPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
