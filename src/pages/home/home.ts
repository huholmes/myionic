import { PayMethodPage } from './../pay-method/pay-method';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController) {

  }
  goToPayMethod(){
    this.navCtrl.push(PayMethodPage);
  }
}
