import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckInVoucherPage } from './check-in-voucher';

@NgModule({
  declarations: [
    CheckInVoucherPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckInVoucherPage),
  ],
})
export class CheckInVoucherPageModule {}
