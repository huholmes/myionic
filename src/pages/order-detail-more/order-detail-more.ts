import { CheckInVoucherPage } from './../check-in-voucher/check-in-voucher';
import { InvoicePage } from './../invoice/invoice';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConfirmationPage } from '../confirmation/confirmation';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the OrderDetailMorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-order-detail-more',
  templateUrl: 'order-detail-more.html',
})
export class OrderDetailMorePage {


  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }
  goToConfirm() {
    this.navCtrl.push(ConfirmationPage);
  }
  goToInvoice() {
    this.navCtrl.push(InvoicePage);
  }
  goToVoucher() {
    this.navCtrl.push(CheckInVoucherPage);
  }
  action() {
    let total =2254.73;
    let confirm = this.alertCtrl.create();
    confirm.setTitle('取消订单');
    confirm.setSubTitle('<p>依据预订时同意的取消政策，取消后会立刻退回以下的款项。不同支付渠道到账时间略有不同，预计1-7个工作日到账，请耐心等待。</p>')
    confirm.setMessage(`&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收费总额: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="giveTest">${total}元</span><br>
    取消预订费用: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="giveTest">0.00元</span><br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;退款全额: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="giveTest">${total}元</span><br>`)

    confirm.addButton('取消').setCssClass('turnBlack');
    confirm.addButton('确认');
    // confirm.setCssClass('giveTest');
    confirm.present();
  }


}


