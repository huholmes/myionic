import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderDetailMorePage } from './order-detail-more';

@NgModule({
  declarations: [
    OrderDetailMorePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderDetailMorePage),
  ],
})
export class OrderDetailMorePageModule {}
