import { OrderDetailPage } from './../order-detail/order-detail';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PayMethodPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-pay-method',
  templateUrl: 'pay-method.html',
})
export class PayMethodPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  goToOrderDetail(){
    this.navCtrl.push(OrderDetailPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PayMethodPage');
  }

}
