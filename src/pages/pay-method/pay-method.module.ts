import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayMethodPage } from './pay-method';

@NgModule({
  declarations: [
    PayMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(PayMethodPage),
  ],
})
export class PayMethodPageModule {}
